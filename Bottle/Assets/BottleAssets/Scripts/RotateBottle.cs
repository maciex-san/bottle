﻿using UnityEngine;
using System.Collections;

public class RotateBottle : MonoBehaviour {

    private float rotationPower = 0;
    private float speed = 2;
    private bool isAdWatched = false;

    void OnGUI() {
        if (rotationPower > 0) {
            float rotationZ = Time.deltaTime * speed * rotationPower;
            transform.Rotate(0, 0, rotationZ);
            rotationPower -= Time.deltaTime * 10;
        } else if (!isAdWatched) {
            FindObjectOfType<Ads>().WachAdd();
            isAdWatched = true;
        }
    }

    public void RotateBottleRandom() {
        rotationPower = Random.Range(50, 100);
        isAdWatched = 0 == Random.Range(0, 5);
    }
}
